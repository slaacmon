--
--  Copyright (C) 2023  Reto Buerki <reet@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Alog.Logger;
with Alog.Dst_Filter;
with Alog.Facilities.Syslog;

package body SLAAC
is

   Logger : Alog.Logger.Instance (Init => False);

   -------------------------------------------------------------------------

   procedure Log
     (Msg   : String;
      Level : Alog.Log_Level := Alog.Info)
   is
   begin
      Logger.Log_Message
        (Level => Level,
         Msg   => Msg);
   end Log;

   -------------------------------------------------------------------------

   procedure Set_Loglevel (Level : Alog.Log_Level)
   is
   begin
      Alog.Dst_Filter.Set_Loglevel
        (Name  => "syslog",
         Level => Level);
   end Set_Loglevel;

begin
   declare
      S : constant Alog.Facilities.Syslog.Handle
        := new Alog.Facilities.Syslog.Instance;
   begin
      S.Set_Name (Name => "syslog");
      S.Toggle_Write_Timestamp (State => False);
      S.Set_Origin (Value => Alog.Facilities.Syslog.LOG_DAEMON);
      Logger.Attach_Facility (Facility => Alog.Facilities.Handle (S));
      Alog.Dst_Filter.Set_Default_Level (Level => Alog.Info);
   end;
end SLAAC;
