--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Command_Line;
with Ada.Exceptions;
with Ada.Strings.Unbounded;

with GNAT.Strings;
with GNAT.Command_Line;

with Interfaces.C.Strings;

with Alog;

with netlink_object_h;
with netlink_route_link_h;

with NL.Cache_Manager;

with SLAAC.Version;
with SLAAC.Handlers;

procedure Slaacmon
is
   Cmdline         : GNAT.Command_Line.Command_Line_Configuration;
   Invalid_Cmdline : exception;
   Iface_Name      : aliased GNAT.Strings.String_Access := null;
   Dbus_Iface      : aliased GNAT.Strings.String_Access := null;
   Debug           : aliased Boolean;

   Sock            : NL.Socket_Type;
   Mngr            : NL.Cache_Manager.Cache_Mngr_Type;
begin
   GNAT.Command_Line.Set_Usage
     (Config => Cmdline,
      Usage  => "-i <network_interface> -b <dbus_iface>",
      Help   => "SLAAC monitor, version " & SLAAC.Version.Version_String);
   GNAT.Command_Line.Define_Switch
     (Config => Cmdline,
      Switch => "-h",
      Help   => "Display usage and exit");
   GNAT.Command_Line.Define_Switch
     (Config => Cmdline,
      Output => Iface_Name'Access,
      Switch => "-i:",
      Help   => "Interface to monitor");
   GNAT.Command_Line.Define_Switch
     (Config => Cmdline,
      Output => Dbus_Iface'Access,
      Switch => "-b:",
      Help   => "D-Bus interface to send updates");
   GNAT.Command_Line.Define_Switch
     (Config => Cmdline,
      Output => Debug'Access,
      Switch => "-d",
      Help   => "Debug mode");

   GNAT.Command_Line.Getopt (Config => Cmdline);
   if Iface_Name.all'Length = 0 or Dbus_Iface.all'Length = 0 then
      raise Invalid_Cmdline;
   end if;

   if Debug then
      SLAAC.Set_Loglevel (Level => Alog.Debug);
   end if;

   NL.Cache_Manager.Initialize
     (Cache_Mngr => Mngr,
      Socket     => Sock);

   declare
      Cache : constant access netlink_object_h.nl_cache
        := Mngr.Get_Link_Cache;
      C_Name : Interfaces.C.Strings.chars_ptr
        := Interfaces.C.Strings.New_String (Str => Iface_Name.all);
      Link : constant access netlink_route_link_h.rtnl_link
        := netlink_route_link_h.rtnl_link_get_by_name
          (arg1 => Cache,
           arg2 => C_Name);
   begin
      Interfaces.C.Strings.Free (Item => C_Name);

      if Link = null then
         SLAAC.Log (Msg => "Invalid interface '" & Iface_Name.all & "'");
         Ada.Command_Line.Set_Exit_Status (Ada.Command_Line.Failure);
         return;
      end if;

      SLAAC.Handlers.Instance.Iface_Idx
        := netlink_route_link_h.rtnl_link_get_ifindex (arg1 => Link);
      netlink_route_link_h.rtnl_link_put (arg1 => Link);
   end;

   SLAAC.Handlers.Instance.Dbus_Iface
     := Ada.Strings.Unbounded.To_Unbounded_String (Dbus_Iface.all);

   SLAAC.Log (Msg => "SLAACmon version "
              & SLAAC.Version.Version_String & " starting");
   SLAAC.Log (Msg => "Observing SLAAC of iface "
              & Iface_Name.all & " with idx"
              & SLAAC.Handlers.Instance.Iface_Idx'Img);

   Mngr.Register_Link_Handler  (Handler => SLAAC.Handlers.Instance'Access);
   Mngr.Register_Addr_Handler  (Handler => SLAAC.Handlers.Instance'Access);
   Mngr.Register_Route_Handler (Handler => SLAAC.Handlers.Instance'Access);

   SLAAC.Log (Msg   => "Sending the ready signal",
              Level => Alog.Debug);
   SLAAC.Handlers.Instance.Signal_Ready;

   loop
      Mngr.Peek;
   end loop;

exception
   when Invalid_Cmdline |
        GNAT.Command_Line.Invalid_Switch |
        GNAT.Command_Line.Invalid_Parameter =>
      SLAAC.Log (Msg   => "Invalid command line (try --help)",
                 Level => Alog.Warning);
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
   when E : others =>
      SLAAC.Log (Msg   => "Terminating due to error",
                 Level => Alog.Error);
      SLAAC.Log (Msg   => Ada.Exceptions.Exception_Information (X => E),
                 Level => Alog.Error);
end Slaacmon;
