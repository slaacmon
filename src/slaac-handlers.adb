--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

pragma Warnings (Off);
with System.OS_Constants;
pragma Warnings (On);

with Ada.Strings.Fixed;

with D_Bus.Types;
with D_Bus.Arguments.Basic;
with D_Bus.Arguments.Containers;

with netlink_route_link_inet6_h;
with netlink_route_nexthop_h;
with netlink_addr_h;
with linux_rtnetlink_h;
with x86_64_linux_gnu_bits_stdint_uintn_h;

with NL.Utils;

package body SLAAC.Handlers
is

   use Ada.Strings.Unbounded;
   use type Interfaces.C.int;
   use type NL.Cache_Actions_Type;

   type RA_State_Type is record
      Received  : Boolean := False;
      Managed   : Boolean := False;
      Otherconf : Boolean := False;
   end record;

   RA_State : RA_State_Type;

   --  Convert given string to D-Bus byte array.
   function To_Byte_Array
     (Str : String)
      return D_Bus.Arguments.Containers.Array_Type;

   -------------------------------------------------------------------------

   procedure Address_Update
     (H      : in out My_Handler_Type;
      Action :        NL.Cache_Actions_Type;
      Msg    : access netlink_route_addr_h.rtnl_addr)
   is
      Idx    : constant Interfaces.C.int
        := netlink_route_addr_h.rtnl_addr_get_ifindex (arg1 => Msg);
      Scope  : constant NL.Scope_Type
        := NL.Scope_Type'Enum_Val
          (netlink_route_addr_h.rtnl_addr_get_scope
             (arg1 => Msg));
      Family : constant Interfaces.C.int
        := netlink_route_addr_h.rtnl_addr_get_family
          (arg1 => Msg);
   begin
      SLAAC.Log (Msg   => "Address: " & Action'Img & ", idx" & Idx'Img
                 & ", scope " & Scope'Img & ", family" & Family'Img,
                 Level => Alog.Debug);

      if Action = NL.NL_Act_New
        and H.Iface_Idx = Idx
        and Family = System.OS_Constants.AF_INET6
        and Scope in NL.RT_Scope_Link | NL.RT_Scope_Site | NL.RT_Scope_Universe
      then
         declare
            use type D_Bus.Types.Obj_Path;
            use D_Bus.Arguments.Basic;
            use D_Bus.Arguments.Containers;

            Addr : constant String
              := NL.Utils.Nladdr_To_String
                (Addr => netlink_route_addr_h.rtnl_addr_get_local
                   (arg1 => Msg));
            Netmask : constant String
              := Ada.Strings.Fixed.Trim
                (Side   => Ada.Strings.Left,
                 Source => netlink_route_addr_h.rtnl_addr_get_prefixlen
                   (arg1 => Msg)'Img);
            Args : D_Bus.Arguments.Containers.Array_Type;
         begin
            SLAAC.Log (Msg => "New IPv6 address appeared");
            SLAAC.Log (Msg => " Address : " & Addr);
            SLAAC.Log (Msg => " Netmask : " & Netmask);
            SLAAC.Log (Msg => " Scope   : " & Scope'Img);

            Args.Append
              (New_Item => Create
                 (Key   => +"reason",
                  Value => Create
                    (Source => To_Byte_Array
                         (Str => "new_address"))));
            Args.Append
              (New_Item => Create
                 (Key   => +"addr",
                  Value => Create
                    (Source => To_Byte_Array
                         (Str => Addr))));
            Args.Append
              (New_Item => Create
                 (Key   => +"netmask",
                  Value => Create
                    (Source => To_Byte_Array
                         (Str => Netmask))));
            Args.Append
              (New_Item => Create
                 (Key   => +"scope",
                  Value => Create
                    (Source => To_Byte_Array
                         (Str => Scope'Img))));

            D_Bus.Connection.Send_Signal
              (Connection  => H.DBus_Conn,
               Object_Name => +"/",
               Iface       => To_String (H.Dbus_Iface),
               Name        => "Event",
               Args        => +Args);
         end;
      end if;
   end Address_Update;

   -------------------------------------------------------------------------

   procedure Link_Update
     (H      : in out My_Handler_Type;
      Action :        NL.Cache_Actions_Type;
      Msg    : access netlink_route_link_h.rtnl_link)
   is
      Idx : constant Interfaces.C.int
        := netlink_route_link_h.rtnl_link_get_ifindex (arg1 => Msg);
   begin
      SLAAC.Log (Msg   => "Link: " & Action'Img & ", idx" & Idx'Img,
                 Level => Alog.Debug);

      if Action in NL.NL_Act_New | NL.NL_Act_Change
        and H.Iface_Idx = Idx
      then
         declare
            use type Interfaces.C.unsigned;

            Inet6_Flags : aliased
              x86_64_linux_gnu_bits_stdint_uintn_h.uint32_t;
            Inet6_Flags_Status : constant Interfaces.C.int
              := netlink_route_link_inet6_h.rtnl_link_inet6_get_flags
                (arg1 => Msg,
                 arg2 => Inet6_Flags'Access);
            New_RA_State : RA_State_Type;
         begin
            if Inet6_Flags_Status = 0 then
               SLAAC.Log
                 (Msg   => "Link: Inet6_Flags" & Inet6_Flags'Img,
                  Level => Alog.Debug);
               New_RA_State :=
                 (Received  => True,
                  Managed   => (Inet6_Flags and NL.IF_RA_MANAGED) /= 0,
                  Otherconf => (Inet6_Flags and NL.IF_RA_OTHERCONF) /= 0);
               if New_RA_State /= RA_State then
                  RA_State := New_RA_State;

                  SLAAC.Log (Msg => "RA: Managed " & RA_State.Managed'Img
                             & " otherconf " & RA_State.Otherconf'Img);

                  declare
                     use type D_Bus.Types.Obj_Path;
                     use D_Bus.Arguments.Basic;
                     use D_Bus.Arguments.Containers;

                     Args : D_Bus.Arguments.Containers.Array_Type;
                  begin
                     Args.Append
                       (New_Item => Create
                          (Key   => +"reason",
                           Value => Create
                             (Source => To_Byte_Array
                                  (Str => "link_changed"))));
                     Args.Append
                       (New_Item => Create
                          (Key   => +"managed",
                           Value => Create
                             (Source => To_Byte_Array
                                  (Str => RA_State.Managed'Img))));
                     Args.Append
                       (New_Item => Create
                          (Key   => +"otherconf",
                           Value => Create
                             (Source => To_Byte_Array
                                  (Str => RA_State.Otherconf'Img))));
                     D_Bus.Connection.Send_Signal
                       (Connection  => H.DBus_Conn,
                        Object_Name => +"/",
                        Iface       => To_String (H.Dbus_Iface),
                        Name        => "Event",
                        Args        => +Args);
                  end;
               end if;
            end if;
         end;
      end if;
   end Link_Update;

   -------------------------------------------------------------------------

   procedure Route_Update
     (H      : in out My_Handler_Type;
      Action :        NL.Cache_Actions_Type;
      Msg    : access netlink_route_route_h.rtnl_route)
   is
      use type Interfaces.C.unsigned;
      use type Interfaces.C.unsigned_char;

      Table     : constant Interfaces.C.unsigned
        := netlink_route_route_h.rtnl_route_get_table
          (arg1 => Msg);
      Family    : constant Interfaces.C.unsigned_char
        := netlink_route_route_h.rtnl_route_get_family
          (arg1 => Msg);
      Hop_Count : constant Interfaces.C.int
        := netlink_route_route_h.rtnl_route_get_nnexthops
          (arg1 => Msg);
   begin
      SLAAC.Log (Msg   => "Route: " & Action'Img & ", table" & Table'Img
                 & ", family" & Family'Img & ", hop_count" & Hop_Count'Img,
                 Level => Alog.Debug);

      if Action = NL.NL_Act_New
        and Table = linux_rtnetlink_h.RT_TABLE_MAIN
        and Family = System.OS_Constants.AF_INET6
        and Hop_Count > 0
      then
         for I in 0 .. Hop_Count - 1 loop
            declare
               Next_Hop : constant access
                 netlink_route_nexthop_h.rtnl_nexthop
                   := netlink_route_route_h.rtnl_route_nexthop_n
                     (arg1 => Msg,
                      arg2 => I);
               Gw : constant access netlink_addr_h.nl_addr
                 := netlink_route_nexthop_h.rtnl_route_nh_get_gateway
                   (arg1 => Next_Hop);
               If_Idx : constant Interfaces.C.int
                 := netlink_route_nexthop_h.rtnl_route_nh_get_ifindex
                   (arg1 => Next_Hop);
            begin
               SLAAC.Log (Msg   => "Route: for idx" & If_Idx'Img & ", gw "
                          & Boolean'Image (Gw /= null),
                          Level => Alog.Debug);

               if If_Idx = H.Iface_Idx
                 and then Gw /= null
               then
                  declare
                     use type D_Bus.Types.Obj_Path;
                     use D_Bus.Arguments.Basic;
                     use D_Bus.Arguments.Containers;

                     Gateway : constant String := NL.Utils.Nladdr_To_String
                       (Addr => Gw);
                     Args    : D_Bus.Arguments.Containers.Array_Type;
                  begin
                     SLAAC.Log (Msg => "New route with gateway " & Gateway);

                     Args.Append
                       (New_Item => Create
                          (Key   => +"reason",
                           Value => Create
                             (Source => To_Byte_Array
                                  (Str => "new_route"))));
                     Args.Append
                       (New_Item => Create
                          (Key   => +"gateway",
                           Value => Create
                             (Source => To_Byte_Array
                                  (Str => Gateway))));

                     D_Bus.Connection.Send_Signal
                       (Connection  => H.DBus_Conn,
                        Object_Name => +"/",
                        Iface       => To_String (H.Dbus_Iface),
                        Name        => "Event",
                        Args        => +Args);
                  end;
               end if;
            end;
         end loop;
      end if;
   end Route_Update;

   -------------------------------------------------------------------------

   procedure Signal_Ready (H : My_Handler_Type)
   is
      use type D_Bus.Types.Obj_Path;
      use D_Bus.Arguments.Basic;
      use D_Bus.Arguments.Containers;

      Args : D_Bus.Arguments.Containers.Array_Type;
   begin
      Args.Append
        (New_Item => Create
           (Key   => +"reason",
            Value => Create
              (Source => To_Byte_Array
                   (Str => "ready"))));
      D_Bus.Connection.Send_Signal
        (Connection  => H.DBus_Conn,
         Object_Name => +"/",
         Iface       => To_String (H.Dbus_Iface),
         Name        => "Event",
         Args        => +Args);
   end Signal_Ready;

   -------------------------------------------------------------------------

   function To_Byte_Array
     (Str : String)
      return D_Bus.Arguments.Containers.Array_Type
   is
      use D_Bus.Arguments.Basic;

      Bytes : D_Bus.Arguments.Containers.Array_Type;
   begin
      for B in Str'Range loop
         Bytes.Append (New_Item => +D_Bus.Byte'(Character'Pos (Str (B))));
      end loop;

      return Bytes;
   end To_Byte_Array;

end SLAAC.Handlers;
