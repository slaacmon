--
--  Copyright (C) 2020  Reto Buerki <reet@codelabs.ch>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Strings.Unbounded;

with Interfaces.C;

with D_Bus.Connection;

with netlink_route_link_h;
with netlink_route_addr_h;
with netlink_route_route_h;

with NL.Cache_Manager;

package SLAAC.Handlers
is

   type My_Handler_Type is new NL.Cache_Manager.Link_Handler_Type
     and NL.Cache_Manager.Addr_Handler_Type
     and NL.Cache_Manager.Route_Handler_Type with record
      Iface_Idx  : Interfaces.C.int;
      Dbus_Iface : Ada.Strings.Unbounded.Unbounded_String;
      DBus_Conn  : D_Bus.Connection.Connection_Type
        := D_Bus.Connection.Connect (Bus => D_Bus.Bus_System);
   end record;

   Conversion_Error : exception;

   procedure Signal_Ready (H : My_Handler_Type);

   overriding
   procedure Link_Update
     (H      : in out My_Handler_Type;
      Action :        NL.Cache_Actions_Type;
      Msg    : access netlink_route_link_h.rtnl_link);

   overriding
   procedure Address_Update
     (H      : in out My_Handler_Type;
      Action :        NL.Cache_Actions_Type;
      Msg    : access netlink_route_addr_h.rtnl_addr);

   overriding
   procedure Route_Update
     (H      : in out My_Handler_Type;
      Action :        NL.Cache_Actions_Type;
      Msg    : access netlink_route_route_h.rtnl_route);

   Instance : aliased My_Handler_Type;

end SLAAC.Handlers;
