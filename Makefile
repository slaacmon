BIN_DIR = bin
OBJ_DIR = obj
DESTDIR = /usr/local

VERSION_SPEC  = src/slaac-version.ads
VERSION       = $(shell cat .version | sed 's/^v//')
GIT_REV      := $(shell git describe --always --dirty="-UNCLEAN" 2> /dev/null)

NUM_CPUS   := $(shell getconf _NPROCESSORS_ONLN)
GMAKE_OPTS ?= -p -j$(NUM_CPUS)

all: build

.version: FORCE
	@if [ -d .git ]; then \
		if [ -r $@ ]; then \
			if [ "$$(cat $@)" != "$(GIT_REV)" ]; then \
				echo $(GIT_REV) > $@; \
			fi; \
		else \
			echo $(GIT_REV) > $@; \
		fi \
	fi

$(VERSION_SPEC): .version
	@echo "package SLAAC.Version is"                > $@
	@echo "   Version_String : constant String :=" >> $@
	@echo "     \"$(VERSION)\";"                   >> $@
	@echo "end SLAAC.Version;"                     >> $@

build: $(VERSION_SPEC)
	@gprbuild $(GMAKE_OPTS) -Pslaacmon.gpr

install: build
	install -m 2775 -d $(DESTDIR)/bin
	install $(BIN_DIR)/slaacmon $(DESTDIR)/bin

clean:
	@rm -rf $(BIN_DIR)
	@rm -rf $(OBJ_DIR)

FORCE:
